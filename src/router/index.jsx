// react
import { BrowserRouter, Navigate, useRoutes,  } from 'react-router-dom';

// layout
import Layout from '@/layout/index.jsx';

// views
import Calculator from '@/views/calculator/index.jsx';
import ErrorPage from '@/views/errorPage/errorPage.jsx';

// base router
const constantRoutes = [
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                path: '/',
                element: <Calculator />
            }
        ]
    },
    {
        path: '/404',
        element: <Layout />,
        children: [
            {
                path: '/404',
                element: <ErrorPage />
            }
        ]
    },
    {
        path: '*',
        element: <Navigate to='/404' />
    }
];

// appRouter
const AppRouter = () => {
    const routes = useRoutes(constantRoutes);
    
    return routes;
};

// routers
const Router = () => {
    return (
        <BrowserRouter>
            <AppRouter></AppRouter>
        </BrowserRouter>
    );
};

export default Router;